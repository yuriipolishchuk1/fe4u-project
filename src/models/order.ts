import type { Seat } from "./show";

export interface Order {
    showId: number,
    name: string,
    email: string,
    seats: Seat[]
}