export interface Cinema {
    id: number,
    cityId: number,
    address: string
}