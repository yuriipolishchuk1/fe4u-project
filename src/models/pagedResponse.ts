export interface PagedResponse<T> {
    total: number,
    body: T[]
}