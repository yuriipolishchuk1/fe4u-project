export interface Film {
	id: number,
	title: string,
	year: number,
	poster: string,
	ageRating: number,
}
