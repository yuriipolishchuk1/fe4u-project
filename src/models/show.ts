import type { Dayjs } from "dayjs";

export interface Show {
    id: number,
    filmId: number,
    technology: Technology,
    time: Dayjs
}

export type Technology = '2D' | '3D' | 'IMAX';


export interface Seat {
    row: number,
    seat: number,
    price: number,
    isAvailable: boolean
}