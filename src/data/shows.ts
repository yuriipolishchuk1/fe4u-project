import dayjs from "dayjs";
import type { Show } from "../models/show";

export const showsList: Show[] = [{
    id: 1,
    filmId: 1,
    technology: '2D',
    time: dayjs().date(10).month(11).year(2021).hour(10).minute(45)
},
{
    id: 2,
    filmId: 1,
    technology: 'IMAX',
    time: dayjs().date(10).month(11).year(2021).hour(11).minute(20)
},
{
    id: 3,
    filmId: 1,
    technology: '3D',
    time: dayjs().date(11).month(11).year(2021).hour(13).minute(30)
},
{
    id: 4,
    filmId: 2,
    technology: 'IMAX',
    time: dayjs().date(10).month(11).year(2021).hour(15).minute(20)
},
{
    id: 5,
    filmId: 2,
    technology: '3D',
    time: dayjs().date(11).month(11).year(2021).hour(13).minute(45)
},
{
    id: 6,
    filmId: 3,
    technology: '2D',
    time: dayjs().date(10).month(11).year(2021).hour(10).minute(25)
},
{
    id: 7,
    filmId: 3,
    technology: 'IMAX',
    time: dayjs().date(11).month(11).year(2021).hour(14).minute(20)
},];