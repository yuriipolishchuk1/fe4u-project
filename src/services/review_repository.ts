import type { Comment } from "../models/comment";
import { getFilms } from "./film_repository";

const reviews = new Map<number, Comment[]>();

getFilms({skip: 0, take: Number.MAX_SAFE_INTEGER}).body.forEach(f => reviews.set(f.id, []));

export function getComments(filmId: number){
    return reviews.get(filmId);
}

export function postComment(filmId: number, comment: Comment){
    reviews.get(filmId).push(comment);
}