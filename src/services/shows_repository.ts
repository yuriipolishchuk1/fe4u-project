import dayjs from "dayjs";
import type { Seat, Show, Technology } from "../models/show";
import { showsList } from "../data/shows";

const shows = new Map<number, Show>(showsList.map(s => [s.id, s]));
const seats = new Map<number, Seat[][]>();

showsList.forEach(s => seats.set(s.id, initSeats()));

export function getShows(filter: ShowFilter) {
    return Array.from(shows.values())
        .filter(s => {
            if (filter.filmId && s.filmId !== filter.filmId)
                return false;
            if (filter.technologies && filter.technologies.every(t => t !== s.technology))
                return false;
            return true;
        });
}

export function getShow(id: number){
    return shows.get(id);
}

export interface ShowFilter {
    filmId?: number,
    technologies?: Technology[]
}

function initSeats(){
    const rows = [];
    for (let i = 0; i < 8; i++) {
        const row: Seat[] = [];
        for (let j = 0; j < 8; j++) {
            row[j] = { row: i+1, seat: j+1, isAvailable: true, price: 100 };
        }
        rows[i] = row;
    }
    return rows;
}

export function getSeats(showId: number): Seat[][] {
    return seats.get(showId);
}

export function bookSeats(showId: number, bookedSeats: Seat[]){
    const showSeats = seats.get(showId);
    bookedSeats.forEach(s => showSeats[s.row - 1][s.seat - 1].isAvailable = false);
}