import type { Film } from '../models/film';
import type { Pagination } from '../models/pagination';
import { sortArray } from '../utils';

import json from '../data/films.json';
import type { PagedResponse } from '../models/pagedResponse';

const films = new Map<number, Film>(json.map(f => [f.id, f]));

export const getFilm = (id: number) => films.get(id);
export const getFilms = (query: FilmParams & Pagination): PagedResponse<Film> => {
	let results = Array.from(films.values())
		.filter(film => {
			if (query.ageRating && film.ageRating > parseInt(query.ageRating)) return false;
			if (query.search && !film.title.toLowerCase().includes(query.search.toLowerCase())) return false;
			return true;
		});
	if (query.sort) {
		results = sortArray(results, query.sort);
	}
	const total = results.length;
	const startIndex = query.skip ? query.skip - 1 : 0;
	const endIndex = startIndex + (query.take || 10);
	return {total, body: results.slice(startIndex, endIndex)};
};

export type FilmSort = `${'title' | 'year'} ${'asc' | 'desc'}`;
export interface FilmParams {
	ageRating?: string,
	search?: string,
	sort?: FilmSort
}