export function groupBy<T, K>(list: T[], keyGetter: (arg0: T) => K): Map<K, T[]> {
    const map = new Map();
    list.forEach((item) => {
        const key = keyGetter(item);
        const collection = map.get(key);
        if (!collection) {
            map.set(key, [item]);
        } else {
            collection.push(item);
        }
    });
    return map;
}

type SortParams = `${string} ${'asc'|'desc'}`

export function sortArray<T>(array: T[], sort: SortParams){
    if (array.length > 0){
        const [field, direction] = sort.split(' ');
        let sortFunc;
        if (isString(array[0][field])){
            sortFunc = (u1, u2) => {
                const comp = u1[field].localeCompare(u2[field]);
                return direction === 'asc' ? comp : -comp;
            };
        } else {
            sortFunc = (u1, u2) => {
                if (u1[field] < u2[field]) return direction === 'asc' ? -1 : 1;
                if (u1[field] > u2[field]) return direction === 'asc' ? 1 : -1;
                return 0;
            };
        }
        return array.sort(sortFunc);
    }
    return array;
}

function isString(s: any): s is string {
    return typeof s === 'string' || s instanceof String;
}

export async function get<T>(fetch: (info: RequestInfo, init?: RequestInit) => Promise<Response>, url: string): Promise<T>{
    const response = await fetch(url);
    const data = await response.json();
    return data as T;
}

export function filterEmpty(obj) {
    return Object.fromEntries(Object.entries(obj).filter(([k, v]) => v !== '' && v !== undefined && v !== null));
}