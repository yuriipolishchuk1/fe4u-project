import type { Film } from '../models/film';
import { getFilms } from '../services/film_repository';

export async function get({ params }): Promise<{body: Film[]}> {
	return {
		body: getFilms({skip: 0, take: 3}).body
	};
}
