import type { Film } from '../../models/film';
import type { Seat } from '../../models/show';
import { getFilm } from '../../services/film_repository';
import { getSeats, getShow } from '../../services/shows_repository';

export async function get({ params }): Promise<{ body: Seat[][] }> {
    return {
        body: getSeats(parseInt(params.id))
    };
}
