import type { Order } from "../../models/order";
import { bookSeats } from "../../services/shows_repository";

export function post({ body }) {
    bookSeats(body.showId, body.seats);
    return { status: 200 }
}