import qs from 'qs';
import type { Film } from '../models/film';
import type { PagedResponse } from '../models/pagedResponse';
import { FilmParams, getFilms } from '../services/film_repository';

export async function get({ params, query }): Promise<{body: PagedResponse<Film>}> {
	const queryParams = qs.parse(query.toString());
	const skip = queryParams.page ? (parseInt(queryParams.page as string) - 1) * 10 : 0;
	return {
		body: getFilms({...queryParams, skip, take: 10})
	};
}
