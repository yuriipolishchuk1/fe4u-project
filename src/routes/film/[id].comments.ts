import type { Comment } from '../../models/comment';
import { getComments, postComment } from '../../services/review_repository';

export async function get({ params }): Promise<{ body: Comment[] }> {
    return {
        body: getComments(parseInt(params.id))
    };
}

export function post({ params, body }) {
    postComment(parseInt(params.id), body);
    return { status: 200 }
}