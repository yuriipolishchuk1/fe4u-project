import type { Film } from '../../models/film';
import { getFilm } from '../../services/film_repository';

export async function get({ params }): Promise<{ body: Film }> {
	return {
		body: getFilm(parseInt(params.id))
	};
}
