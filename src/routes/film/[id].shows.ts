import qs from 'qs';
import type { Film } from '../../models/film';
import type { Show } from '../../models/show';
import { getFilm } from '../../services/film_repository';
import { getShows, ShowFilter } from '../../services/shows_repository';

export async function get({ params, query }): Promise<{ body: Show[] }> {
    const filter = qs.parse(query.toString(), {plainObjects: true, parseArrays: true}) as ShowFilter;
	return {
		body: getShows({filmId: parseInt(params.id), ...filter})
	};
}
